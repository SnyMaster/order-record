/**
 * Created by Serg on 11.07.2021.
 */

import { LightningElement, wire, api} from 'lwc';
import getOrderProductsList from '@salesforce/apex/ProductsListController.getListProduct';
import { refreshApex } from '@salesforce/apex';

import { subscribe, MessageContext } from 'lightning/messageService';
import ORDER_UPDATED_CHANNEL from '@salesforce/messageChannel/messageUpdateOrderproducts__c';

const columns = [
     { label: 'Name', fieldName: 'Product2Name', type: 'text', hideDefaultActions: true },
     { label: 'Unit price', fieldName: 'UnitPrice', type: 'text', hideDefaultActions: true, initialWidth: 120 },
     { label: 'Quantity', fieldName: 'Quantity', type: 'number', hideDefaultActions: true, initialWidth: 80 },
     { label: 'Total Price', fieldName: 'TotalPrice', type: 'currency', hideDefaultActions: true, initialWidth: 180 }
];

export default class ProductsList extends LightningElement {
    @api recordId;

    subscription = null;
    errors;
    columns = columns;

    @wire(MessageContext)
    messageContext;

    orderItemsResult;

    @wire(getOrderProductsList, {orderId : '$recordId'})
    orderItemsResult;

    get orderProductsList() {
        if (this.orderItemsResult.data == undefined) {
            return [];
        }
        return  this.orderItemsResult.data.map(item => {
                return {
                        Id: item.Id,
                        Product2Name: String(item.Product2.Name),
                        UnitPrice: item.UnitPrice,
                        Quantity: item.Quantity,
                        TotalPrice: item.TotalPrice
                    }
            });
    }

    subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            ORDER_UPDATED_CHANNEL,
            (message) => this.handleMessage(message)
        );
    }

    handleMessage(message) {
        return refreshApex(this.orderItemsResult);
    }

    connectedCallback() {
        this.subscribeToMessageChannel();
    }
}