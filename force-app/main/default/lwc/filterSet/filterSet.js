/**
 * Created by Serg on 11.07.2021.
 */

import { LightningElement, api } from 'lwc';

export default class FilterSet extends LightningElement {
    isFilterHide = true;
    filterValue;

    handleFilterChange(event) {
        this.filterValue = event.target.value;
    }
    handleFilterSwitch() {
        this.isFilterHide = !this.isFilterHide;
    }

    handleApplyFilter() {
        this.dispatchEvent(new CustomEvent('setpricebookfilter', { detail: {searchName: this.filterValue} }));
    }

    get filterLabel() {
        return `${this.isFilterHide ? 'Show' : 'Hide'} filter
                ${this.filterValue && this.isFilterHide ? `(selected by "${this.filterValue}")` : ''}`;
    }

}