/**
 * Created by Serg on 11.07.2021.
 */

import { LightningElement, api, wire } from 'lwc';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import { reduceErrors } from 'c/ldsUtils'
import getPricebookEntries from '@salesforce/apex/PricebookEntryListController.getPriceBookEntries';
import addProductsToOrder from '@salesforce/apex/PricebookEntryListController.addProductsToOrder';
import getPriceBookEntriesCount from '@salesforce/apex/PricebookEntryListController.getPriceBookEntriesCount';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import { publish, MessageContext } from 'lightning/messageService';
import ORDER_UPDATED_CHANNEL from '@salesforce/messageChannel/messageUpdateOrderproducts__c';

const columns = [
     { label: 'Name', fieldName: 'Product2Name', type: 'text', hideDefaultActions: true },
     { label: 'Unit price', fieldName: 'UnitPrice', type: 'currency', initialWidth: 120, hideDefaultActions: true },
];

export default class ProductBookList extends LightningElement {
    @api recordId;
    @api heightComponent;
    @api typeLazyLoad;

    selectedBookItems = [];
    totalNumberOfRows;
    columns = columns;
    errors;
    productsBookEntry = [];
    isShowListSelected = false;
    selectedItemsTitle = 'Selected items: 0';
    searchName =  null;

    @wire(MessageContext)
    messageContext;

    @wire(getPriceBookEntriesCount, {orderId: '$recordId', searchName : '$searchName'})
    totalNumberOfRows;

    @wire(getPricebookEntries, {orderId: '$recordId',  queryOffset: 0, searchName : '$searchName'})
    wireProductsBookEntries({data, error}) {
        if (error) {
                this.showErrorToast(reduceErrors(error));
        }
        if (data) {
            this.productsBookEntry = this.getLoadedProductsBookEntry(data);
        }
    }

    get isSelectedBookItems() {
        return this.selectedBookItems.length != 0;
    }

    get showedRecordsLabel() {
        return `Found ${this.totalNumberOfRows.data} ` +
            (this.productsBookEntry.length ? `showed ${this.productsBookEntry.length} ` : '') + 'records';
    }

    handleAddProductToOrder() {
        addProductsToOrder({orderId: this.recordId, products: JSON.stringify(this.selectedBookItems)})
            .then((result) => {
                this.clearSelection();
                const payload =  { recordId: this.recordId };``
                getRecordNotifyChange([payload]);
                publish(this.messageContext, ORDER_UPDATED_CHANNEL, payload);
            })
            .catch((error) => {
                this.showErrorToast(reduceErrors(error));
            });
    }

    handleSelectedEntry(event) {
        this.selectedBookItems = Array.from(event.detail.selectedRows, item => {
            return {id: item.Product2Id, name: item.Product2Name, unitPrice: item.UnitPrice, priceBookEntryId: item.Id}
        });
        this.selectedItemsTitle = 'Selected items: ' + this.selectedBookItems.length;
    }

    handleListSelectedSwitch() {
        this.isShowListSelected = !this.isShowListSelected;
    }

    clearSelection() {
        this.template.querySelector('lightning-datatable').selectedRows = [];
        this.selectedBookItems = [];
        this.isShowListSelected = false;
        this.selectedItemsTitle = 'Selected items: 0';
    }

    handleLoadMoreData(event) {
            event.target.isLoading = true;
            const target = event.target;
            getPricebookEntries({orderId: this.recordId, queryOffset: this.productsBookEntry.length, searchName : this.searchName})
                .then((data) => {
                    if (this.productsBookEntry.length >= this.totalNumberOfRows.data) {
                        target.enableInfiniteLoading = false;
                    } else {
                        const currentData = this.productsBookEntry;
                        const loadedData = this.getLoadedProductsBookEntry(data);
                        this.productsBookEntry = [...currentData, ...loadedData];
                    }
                   target.isLoading = false;
                })
                .catch(error => {
                     target.isLoading = false;
                     this.showErrorToast(reduceErrors(error));
                });
    }

    handleSetPricebookFilter(event) {
        if (event.detail.searchName == undefined ) { return; }
        this.productsBookEntry = [];
        this.searchName = event.detail.searchName;
        this.template.querySelector('lightning-datatable').enableInfiniteLoading = true;
        refreshApex(this.productsBookEntry);
        refreshApex(this.totalNumberOfRows);
    }

    showErrorToast(errorMessage) {
            if (Array.isArray(errorMessage)) {
                errorMessage = errorMessage.join(', ');
            }
        const errorEvent = new ShowToastEvent({
            title: 'Error',
            message: errorMessage,
            variant: 'error',
        });
        this.dispatchEvent(errorEvent);
    }

    getLoadedProductsBookEntry(dataLoaded) {
        return dataLoaded.map(entryItem => {
              return {
                  Id: entryItem.Id,
                  Product2Name: String(entryItem.Product2.Name),
                  UnitPrice: entryItem.UnitPrice,
                  Product2Id: entryItem.Product2Id
              }
          });
    }
}
