/**
 * Created by Serg on 12.07.2021.
 */

import { LightningElement, api } from 'lwc';
import activateOrder from '@salesforce/apex/ActivateOrderController.activateOrder';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import { reduceErrors } from 'c/ldsUtils'
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ActivationOrder extends LightningElement {
    @api recordId;

    handlerActivate() {
        this.errors = '';
        activateOrder({orderId: this.recordId})
            .then(result => {
                getRecordNotifyChange([{recordId: this.recordId}]);
            })
            .catch(error => {
                this.showErrorToast(reduceErrors(error));
            })
    }

    showErrorToast(errorMessage) {
            if (Array.isArray(errorMessage)) {
                errorMessage = errorMessage.join(', ');
            }
        const errorEvent = new ShowToastEvent({
            title: 'Error',
            message: errorMessage,
            variant: 'error',
        });
        this.dispatchEvent(errorEvent);
    }
}