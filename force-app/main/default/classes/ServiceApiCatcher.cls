/**
 * Created by Serg on 12.07.2021.
 */

public with sharing class ServiceApiCatcher implements ServiceApiInterface {

    private String endPoint;
    private String body;
    private String method;
    private HttpRequest request;
    Private HttpResponse response;
    private Map<String, String> headers = new Map<String, String>{'Content-Type' => 'application/json'};

    public Boolean sendRequest() {
        this.request = new HttpRequest();
        this.request.setEndpoint(this.endPoint);
        this.request.setMethod('POST');
        for(String headerKey: this.headers.keySet()){
            this.request.setHeader(headerKey, this.headers.get(headerKey));
        }
        this.request.setBody(this.body);
        Http http = new Http();
        this.response = http.send(this.request);
        if (this.response.getStatusCode() != 200) {
            return false;
        }
        return true;
    }

    public ServiceApiInterface setEndPoint(String endPointName) {
        this.endPoint = endPointName;
        return this;
    }

    public ServiceApiInterface setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public ServiceApiInterface addHeaders(Map<String, String> header) {
        this.headers.putAll(header);
        return this;
    }
    public ServiceApiInterface setBody(String body) {
        this.body = body;
        return this;
    }

    public ServiceApiInterface setMethod(String method) {
        this.method = method;
        return this;
    }

}