/**
 * Created by Serg on 12.07.2021.
 */

public class OrderApiWrapper implements ObjectApiWrapperInterface{

    private Order order;
    private List<OrderItem> orderItems {
        get {
            if (this.orderItems == null) {
                this.orderItems = [SELECT Product2.Name, Product2.ProductCode, UnitPrice, Quantity FROM OrderItem WHERE OrderId = :this.order.Id];
            }
            return this.orderItems;
        }
        set;
    }

    public OrderApiWrapper(Id orderId) {
        this.order = [SELECT Id, Account.AccountNumber, OrderNumber, Type, Status  FROM Order WHERE id = :orderId];
    }

    public String getJson() {
        OrderInfo info = new OrderInfo();
        info.accountNumber = this.order.Account.AccountNumber;
        info.orderNumber = this.order.OrderNumber;
        info.type = this.order.Type;
        info.status = this.order.Status;
        info.orderProducts = new List<OrderItemInfo>();
        for(OrderItem item: this.orderItems) {
            OrderItemInfo itemInfo = new OrderItemInfo();
            itemInfo.name = item.Product2.Name;
            itemInfo.code = item.Product2.ProductCode;
            itemInfo.quantity = item.Quantity;
            itemInfo.unitPrice = item.UnitPrice;
            info.orderProducts.add(itemInfo);
        }
        return JSON.serialize(info, false);
    }

    private class OrderInfo {
        public String accountNumber;
        public String orderNumber;
        public String type;
        public String status;
        public  List<OrderItemInfo> orderProducts;
    }

    private class OrderItemInfo {
        public String name;
        public String code;
        public Decimal unitPrice;
        public Decimal quantity;
    }
}