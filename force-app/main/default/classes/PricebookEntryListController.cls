/**
 * Created by Serg on 11.07.2021.
 */

public with sharing class PricebookEntryListController {

    @AuraEnabled(cacheable=true)
    public static List<PricebookEntry> getPriceBookEntries(Id orderId, integer queryOffset, String searchName) {
        try {
            return OrderService.getPriceBookEntries(orderId, queryOffset, searchName);
        } catch (Exception ex) {
            throw new PricebookEntryListController.PricebookEntryListControllerException('Load price book entries error: ' + ex.getMessage());
        }

    }
    @AuraEnabled(cacheable=true)
    public static Integer getPriceBookEntriesCount(Id orderId, String searchName) {
        return OrderService.getPriceBookEntriesCount(orderId, searchName);
    }

    @AuraEnabled
    public static List<Id> addProductsToOrder(Id orderId, String products) {
        try {
            List<OrderService.ProductItem> productsAdd =
                    (List<OrderService.ProductItem>) JSON.deserialize(products, List<OrderService.ProductItem>.class);
            return OrderService.addProductsToOrder(orderId, productsAdd);
        } catch (Exception e) {
            throw new PricebookEntryListControllerException('Error in add products to order: ' + e.getMessage());
        }
    }

    public class PricebookEntryListControllerException extends Exception {
    }
}