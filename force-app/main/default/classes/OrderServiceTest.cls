/**
 * Created by Serg on 12.07.2021.
 */

@IsTest
public class OrderServiceTest {

    @TestSetup
    static void initData() {
        TestFactory.initData();
    }

    @IsTest
    static void testOrderProduct() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        List<OrderItem> orderItems = OrderService.getOrderProductsList(order.Id);
        System.assert(!orderItems.isEmpty());
    }

    @IsTest
    static void testOrderPriceBookEntries() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        List<PricebookEntry> items = OrderService.getPriceBookEntries(order.Id, 0, 'Test');
        System.assert(!items.isEmpty());
    }

    @IsTest
    static void testPriceBookEntriesCount() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        System.assert(OrderService.getPriceBookEntriesCount(order.Id, 'Test') > 0);
    }

    @IsTest
    static void testSetOrderStatus() {
        Order order = [SELECT Id, Status FROM Order LIMIT 1];
        OrderService.setOrderStatus(order.id, order.Status);
        try {
            OrderService.setOrderStatus(order.id, 'Activated');
        } catch (Exception e) {}
    }

    @IsTest
    static void testAddProductToOrder() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        List<OrderItem> orderItems = OrderService.getOrderProductsList(order.Id);
        System.assert(orderItems[0].Quantity == 1);
        List<PricebookEntry> items = OrderService.getPriceBookEntries(order.Id, 0, 'Test');
        List<OrderService.ProductItem> listProductAdd = new List<OrderService.ProductItem>();
        for (PricebookEntry entry: items) {
            OrderService.ProductItem productItem = new OrderService.ProductItem();
            productItem.priceBookEntryId = entry.Id;
            productItem.id = entry.Product2Id;
            productItem.unitPrice = entry.UnitPrice;
            listProductAdd.add(productItem);
        }
        OrderService.addProductsToOrder(order.id, listProductAdd);
        orderItems = OrderService.getOrderProductsList(order.Id);
        System.assert(orderItems.size() == 2);
        Decimal quantityAll = 0;
        for(OrderItem itemOrder: orderItems) {
            quantityAll += itemOrder.Quantity;
        }
        System.assert(quantityAll == 3);
    }
    @IsTest
    static void testActivateOrder() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        OrderApiWrapper orderWrapper = new OrderApiWrapper(order.id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ActivateOrderCalloutMock(200));
        Test.stopTest();
        System.assert(OrderService.activateOrder(order.id, OrderWrapper));
    }

    @IsTest
    static void testActivateOrderError() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        OrderApiWrapper orderWrapper = new OrderApiWrapper(order.id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ActivateOrderCalloutMock(201));
        Test.stopTest();
        System.assert(!OrderService.activateOrder(order.id, OrderWrapper));
    }
}