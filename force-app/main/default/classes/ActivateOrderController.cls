/**
 * Created by Serg on 12.07.2021.
 */

public with sharing class ActivateOrderController {

    @AuraEnabled
    public static Boolean activateOrder(Id orderId) {
        try {
            OrderApiWrapper orderWrapper = new OrderApiWrapper(orderId);
            if(OrderService.activateOrder(orderId, orderWrapper)) {
                OrderService.setOrderStatus(orderId, 'Activated');
                return  true;
            }
            return false;
        } catch (Exception e){
            throw new ActivationOrderException('Activation error: ' + e.getMessage());
        }
    }

    public class ActivationOrderException extends Exception {}
}