/**
 * Created by Serg on 10.07.2021.
 */

@IsTest
private class ServiceApiCatcherTest {
    @IsTest
    static void testApiCatcher() {
        ActivateOrderCalloutMock calloutMock = new ActivateOrderCalloutMock(200);
        calloutMock
                .setMethod('POST')
                .setHeaders(new Map<String, String>{'Content-Test1' => 'test1'})
                .addHeaders(new Map<String, String>{'Content-Test2' => 'test2'})
                .setBody('testBody');
        Test.setMock(HttpCalloutMock.class, calloutMock);
        Test.startTest();
        ServiceApiCatcher apiCatcher = new ServiceApiCatcher();
        apiCatcher
                .setMethod('POST')
                .setEndPoint('callout:Request_Catcher')
                .setHeaders(new Map<String, String>{'Content-Test1' => 'test1'})
                .addHeaders(new Map<String, String>{'Content-Test2' => 'test2'})
                .setBody('testBody')
                .sendRequest();
        Test.stopTest();
    }
}