/**
 * Created by Serg on 12.07.2021.
 */

public interface ObjectApiWrapperInterface {
    String getJson();
}