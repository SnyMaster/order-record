/**
 * Created by Serg on 11.07.2021.
 */

public with sharing class ProductsListController {

    @AuraEnabled(cacheable=true)
    public static List<OrderItem> getListProduct(Id orderId) {
        return OrderService.getOrderProductsList(orderId);
    }



}