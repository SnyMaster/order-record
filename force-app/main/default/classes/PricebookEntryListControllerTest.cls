/**
 * Created by Serg on 12.07.2021.
 */

@IsTest
private class PricebookEntryListControllerTest {

    @TestSetup
    static void initData() {
        TestFactory.initData();
    }

    @IsTest
    static void testPriceBookEntries() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        List<PricebookEntry> items = PricebookEntryListController.getPriceBookEntries(order.Id, 0, 'Test');
        System.assert(!items.isEmpty());
    }

    @IsTest
    static void testPriceBookEntriesExeption() {
        try {
            PricebookEntryListController.getPriceBookEntries('000000000000AAA', -1, 'Test');
        } catch (Exception ex) {}
    }

    @IsTest
    private static void testPriceBookEntriesCount() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        System.assert(PricebookEntryListController.getPriceBookEntriesCount(order.Id, 'Test') > 0);
    }

    @IsTest
    private static void testPriceBookEntriesCountExeption() {
        String testWhere = '';
        for (Integer i = 0; i < 10000; i++) {
            testWhere = testWhere + ' test';
        }
        PricebookEntryListController.getPriceBookEntriesCount('000000000000AAA', testWhere);
    }

    @IsTest
    private static void testAddProductToOrder() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        List<PricebookEntry> items = PricebookEntryListController.getPriceBookEntries(order.Id, 0,'Test');
        List<OrderService.ProductItem> listProductAdd = new List<OrderService.ProductItem>();
        for (PricebookEntry entry: items) {
            OrderService.ProductItem productItem = new OrderService.ProductItem();
            productItem.priceBookEntryId = entry.Id;
            productItem.id = entry.Product2Id;
            productItem.unitPrice = entry.UnitPrice;
            listProductAdd.add(productItem);
        }
        PricebookEntryListController.addProductsToOrder(order.id, JSON.serialize(listProductAdd));
        List<OrderItem> orderItems = [SELECT Quantity FROM OrderItem WHERE OrderId = :order.Id];
        System.assert(orderItems.size() == 2);
        Decimal quantityAll = 0;
        for(OrderItem itemOrder: orderItems) {
            quantityAll += itemOrder.Quantity;
        }
        System.assert(quantityAll == 3);

        try {
            PricebookEntryListController.addProductsToOrder(order.id, '{}{}');
        } catch (Exception ex) {}
    }
}