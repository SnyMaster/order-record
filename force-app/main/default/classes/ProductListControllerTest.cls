/**
 * Created by Serg on 12.07.2021.
 */

@IsTest
private class ProductListControllerTest {
    @TestSetup
    static void initData() {
        TestFactory.initData();
    }

    @IsTest
    static void testListProduct() {
        Order order = [SELECT Id FROM Order LIMIT 1];
        List<OrderItem> items = ProductsListController.getListProduct(order.id);
        System.assert(!items.isEmpty());
    }
}