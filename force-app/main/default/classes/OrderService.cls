/**
 * Created by Serg on 11.07.2021.
 */

public with sharing class OrderService {

    public static List<OrderItem> getOrderProductsList(Id orderId) {
        return [SELECT Product2.Name, UnitPrice, Quantity, TotalPrice FROM OrderItem WHERE OrderId = :orderId WITH SECURITY_ENFORCED];
    }

    public static  List<PricebookEntry> getPriceBookEntries(Id orderId,  integer queryOffset, String searchName) {
        searchName = getSearchName(searchName);
        return Database.query('SELECT Product2.Name, UnitPrice, Product2Id ' +
                'FROM PricebookEntry ' +
                'WHERE Pricebook2Id IN (SELECT Pricebook2Id FROM Order WHERE Id = :orderId) ' +
                (String.isNotBlank(searchName) ? ' AND Product2.Name LIKE :searchName ' : '') +
                'Order BY Product2.Name ' +
                'LIMIT 5 OFFSET :queryOffset');
    }

    public static Integer getPriceBookEntriesCount(Id orderId, String searchName) {
        searchName = getSearchName(searchName);
        return Database.countQuery('SELECT COUNT() ' +
                'FROM PricebookEntry ' +
                'WHERE Pricebook2Id IN (SELECT Pricebook2Id FROM Order WHERE Id = :orderId) ' +
                (String.isNotBlank(searchName) ? ' AND Product2.Name LIKE :searchName ' : '')
        );
    }

    public static List<Id> addProductsToOrder(Id orderId, List<OrderService.ProductItem> productsAdd) {
        Map<Id, OrderService.ProductItem> productsAddMap = new Map<Id, OrderService.ProductItem>();
        for (OrderService.ProductItem itemProduct: productsAdd) {
            productsAddMap.put(itemProduct.id, itemProduct);
        }
        List<Id> updatedIds = new List<Id>();
        List<OrderItem> orderItems = [SELECT Product2Id, Quantity FROM OrderItem WHERE OrderId = :orderId AND Product2Id IN :productsAddMap.keySet()];
        for (OrderItem itemOrder: orderItems) {
            itemOrder.Quantity += 1;
            productsAddMap.remove(itemOrder.Product2Id);
            updatedIds.add(itemOrder.Id);
        }
        update orderItems;
        List<OrderItem> orderItemsListAdd = new List<OrderItem>();
        for(String productId: productsAddMap.keySet()) {
            OrderItem orderItemAdd = new OrderItem();
            orderItemAdd.OrderId = orderId;
            orderItemAdd.Product2Id = productId;
            orderItemAdd.Quantity = 1;
            orderItemAdd.UnitPrice = productsAddMap.get(productId).unitPrice;
            orderItemAdd.PricebookEntryId = productsAddMap.get(productId).priceBookEntryId;
            orderItemsListAdd.add(orderItemAdd);
        }
        insert orderItemsListAdd;
        for(OrderItem item: orderItemsListAdd) {
            updatedIds.add(item.Id);
        }
        return updatedIds;
    }

    public static Boolean activateOrder(Id orderId, ObjectApiWrapperInterface orderWrapper) {
        ServiceApiCatcher serviceApi = new ServiceApiCatcher();
        return serviceApi
                    .setMethod('POST')
                    .setEndPoint('callout:Request_Catcher')
                    .setHeaders(new Map<String, String>{'Content-Type' => 'application/json'})
                    .setBody(orderWrapper.getJson())
                    .sendRequest();
    }

    public static void setOrderStatus(Id orderId, String newStatus) {
        Order orderItem = [SELECT Status FROM Order WHERE id = :orderId];
        if (OrderItem.Status.equalsIgnoreCase(newStatus)) {
            return;
        }
        orderItem.Status = newStatus;
        update orderItem;
    }


    public class ProductItem {
        public Id id;
        public Decimal unitPrice;
        public String name;
        public Id priceBookEntryId;
    }

    private static  String getSearchName(String searchName) {
        return String.isNotBlank(searchName) ?  searchName + '%' : null;
    }
}
