/**
 * Created by Serg on 12.07.2021.
 */

public interface ServiceApiInterface {
    Boolean sendRequest();
    ServiceApiInterface setEndPoint(String endPointName);
    ServiceApiInterface setHeaders(Map<String, String> headers);
    ServiceApiInterface addHeaders(Map<String, String> headers);
    ServiceApiInterface setBody(String body);
    ServiceApiInterface setMethod(String method);
}