/**
 * Created by Serg on 12.07.2021.
 */

@IsTest
private class ActivateOrderControllerTest {

    @TestSetup
    static void initData() {
        TestFactory.initData();
    }

    @IsTest
    static void testActivate() {
        Order ord = [SELECT Id FROM Order LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ActivateOrderCalloutMock(200));
        Test.stopTest();
        System.assert(ActivateOrderController.activateOrder(ord.Id));
        try {
            ActivateOrderController.activateOrder('000000000000AAA');
        } catch (Exception e) {}
    }

    @IsTest
    static void testActivateError() {
        Order ord = [SELECT Id FROM Order LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ActivateOrderCalloutMock(201));
        Test.stopTest();
        System.assert(!ActivateOrderController.activateOrder(ord.Id));
    }
}