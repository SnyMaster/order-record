/**
 * Created by Serg on 12.07.2021.
 */

@IsTest
global class ActivateOrderCalloutMock implements HttpCalloutMock {
    private Integer statusCode;
    private String body;
    private String method;
    private Map<String, String> headers;

    global ActivateOrderCalloutMock(Integer statusCode) {
        this.statusCode = statusCode;
    }
    global HttpResponse respond(HttpRequest request) {
        this.testRequestParams(request);
        HttpResponse response = new HttpResponse();
        response.setStatusCode(statusCode);
        return response;
    }

    global ActivateOrderCalloutMock setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    global ActivateOrderCalloutMock addHeaders(Map<String, String> header) {
        this.headers.putAll(header);
        return this;
    }
    global ActivateOrderCalloutMock setBody(String body) {
        this.body = body;
        return this;
    }

    global ActivateOrderCalloutMock setMethod(String method) {
        this.method = method;
        return this;
    }

    private void testRequestParams(HttpRequest request) {
        if (this.body != null) {
            System.assert(request.getBody().equals(this.body));
        }
        if (this.method != null) {
            System.assert(request.getMethod().equals(this.method));
        }
        if (this.headers != null) {
            for(String headerKey: this.headers.keySet()){
                System.assertNotEquals(null, request.getHeader(headerKey));
                System.assert(request.getHeader(headerKey).equals(this.headers.get(headerKey)));
            }
        }
    }
}