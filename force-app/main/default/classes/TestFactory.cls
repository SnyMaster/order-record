/**
 * Created by Serg on 12.07.2021.
 */

@IsTest
public class TestFactory {

    public static void initData() {
        Product2 product1 = new Product2();
        product1.Name = 'Test Product 1';
        product1.Description='Test Product';
        product1.productCode = 'AAA-001';
        product1.isActive = true;

        Product2 product2 = new Product2();
        product2.Name = 'Test Product 2';
        product2.Description='Test Product';
        product2.productCode = 'AAA-002';
        product2.isActive = true;

        insert new List<Product2>{product1, product2};

        PricebookEntry standardPrice1 = new PricebookEntry();
        standardPrice1.Pricebook2Id = Test.getStandardPricebookId();
        standardPrice1.Product2Id = product1.Id;
        standardPrice1.UnitPrice = 100;
        standardPrice1.IsActive = true;
        standardPrice1.UseStandardPrice = false;

        PricebookEntry standardPrice2 = new PricebookEntry();
        standardPrice2.Pricebook2Id = Test.getStandardPricebookId();
        standardPrice2.Product2Id = product2.Id;
        standardPrice2.UnitPrice = 100;
        standardPrice2.IsActive = true;
        standardPrice2.UseStandardPrice = false;

        insert new List<PricebookEntry>{standardPrice1, standardPrice2} ;

        Account acc = new Account( Name = 'Test Account' );
        insert acc;

        Order order = new Order(
                AccountId = acc.Id,
                EffectiveDate = System.today(),
                Status = 'Draft',
                PriceBook2Id = Test.getStandardPricebookId()
        );
        insert order;

        OrderItem lineItem = new OrderItem();
        lineItem.OrderId = order.id;
        lineItem.Quantity = 1;
        lineItem.UnitPrice = 100;
        lineItem.Product2id = product1.id;
        lineItem.PricebookEntryId=standardPrice1.id;
        insert lineItem;
    }
}