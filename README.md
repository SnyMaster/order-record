# Record Page Components


## Notes

Communication between components on order record page realised with using Lightning Message Service. It can be also used "pubsub" module  for this. Because this current task do not have limitation for using I prefered LMS way.

Endpoint URL https://great-astro.requestcatcher.com

## Limitations

I have limitations on my org "Lightning Web Component quick actions are not supported for this entity".

## Load sample data

sfdx force:data:tree:import --plan data/org-data-plan.json

sfdx force:apex:execute -f scripts/apex/updatePriceBook.apex